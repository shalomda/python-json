FROM python:2

ARG inBuildNum

ENV build_id = ${inBuildNum}

ADD versionjson.py /
ADD version.json /

# RUN mkdir --parents /VSCode/Test
# RUN pip install pytest

CMD python ./versionjson.py $build_id
