import json
import sys

with open("version.json", "r") as jsonFile:
    data = json.load(jsonFile)

print(data["install"]["version"])
data["install"]["version"] = sys.argv[1]
print(data["install"]["version"])

with open("version.json", "w") as jsonFile:
    json.dump(data, jsonFile)
